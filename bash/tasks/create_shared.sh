#!/bin/bash

# Function to set chmod to +x and execute the input
execute() {
    local file="$1"
    chmod +x "$file"
    "$file"
}

use_test_image=$1
use_local_image=$2

echo "Creating container setup. Parameters - use test image: $use_test_image, use local image: $use_local_image"

# Remove the shared volume if it exists
if [ -d "$SHARED_VOLUME" ]; then
    sudo rm -rf "$SHARED_VOLUME"
    echo "Directory $SHARED_VOLUME has been removed."
fi

# Create the alien-cs.jar
execute "$SCRIPT_DIR/tasks/create_jar.sh"

# Set the shared volume path
export SHARED_VOLUME=$SHARED_VOLUME

# Create SHARED_VOLUME
echo "$SHARED_VOLUME is creating...."
"$JALIEN_SETUP/bin/jared" --jar "$JALIEN/alien-cs.jar" --volume "$SHARED_VOLUME"
echo "SHARED VOLUME : $SHARED_VOLUME created"

container_registry="gitlab-registry.cern.ch/jalien/jalien-setup"
if [ "$use_test_image" = true ]; then
    # use test tag instead of latest tag for images
    echo "Set the image tag to $tag"
    sed -i "s#:latest#:test#g" "$SHARED_VOLUME/docker-compose.yml"
fi

if [ "$use_local_image" = true ]; then
    central_image="jalien-base"
    se_image="xrootd-se"
    worker_image="worker-base"
    ce_image="jalien-ce"
    sed -i "s#image: $container_registry\/$central_image:latest#image: $central_image#g" "$SHARED_VOLUME/docker-compose.yml"
    sed -i "s#image: $container_registry\/$se_image:latest#image: $se_image#g" "$SHARED_VOLUME/docker-compose.yml"
    sed -i "s#image: $container_registry\/$worker_image:latest#image: $worker_image#g" "$SHARED_VOLUME/docker-compose.yml"
    sed -i "s#image: $container_registry\/$ce_image:latest#image: $ce_image#g" "$SHARED_VOLUME/docker-compose.yml"
fi
